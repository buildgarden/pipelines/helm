# helm pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/helm?branch=main)](https://gitlab.com/buildgarden/pipelines/helm/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/helm)](https://gitlab.com/buildgarden/pipelines/helm/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Test, build, and release [Helm](https://helm.sh/) charts.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

This pipeline:

- Lints the chart configuration with
  [helm lint](https://helm.sh/docs/helm/helm_lint/).

- Renders a chart README with
  [helm-docs](https://github.com/norwoodj/helm-docs).

- Publishes the chart to a [ChartMuseum](https://chartmuseum.com/)-compatible
  repository or OCI image registry.

## Setup

Enable the project Container Registry to publish OCI Helm charts.

Enable the project Package Registry to publish ChartMuseum charts.

Create a Helm chart in the `chart/` directory.

## Usage

### Do everything

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-docs.yml
      - helm-lint.yml
      - helm-package.yml
      - helm-push-chartmuseum.yml
      - helm-push-registry.yml
```

### Push to ChartMuseum

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-package.yml
      - helm-push-chartmuseum.yml
```

Specify `HELM_CHART_PATH` if necessary. This pipeline publishes to the GitLab
Package Registry for the current project by default, using a [CI job
token](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html).

### Publish to an OCI registry

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-package.yml
      - helm-push-registry.yml
```

Specify `HELM_CHART_PATH` if necessary. This pipeline publishes to the GitLab
Container Registry for the current project by default, using a [CI job
token](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html).

### Publish Artifact Hub metadata

Create an [`artifacthub-repo.yml`
file](https://github.com/artifacthub/hub/blob/master/docs/metadata/artifacthub-repo.yml),
then include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-push-registry-artifacthub.yml
```

For more documentation, see the [Artifact Hub
docs](https://artifacthub.io/docs/topics/repositories/helm-charts/#oci-support).

### Render a chart README at build time

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-docs.yml
```

### Run `helm lint` on the chart

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-lint.yml
```

### Using charts

See the GitLab documentation on [installing Helm
charts](https://docs.gitlab.com/ee/user/packages/helm_repository/#install-a-package).

Currently, only project-level repositories are supported by GitLab, not
group-level.

## Use cases

### All includes

Add the following to your `.gitlab-ci.yml` file to enable all targets:

```yaml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-autoflake.yml
      - python-black.yml
      - python-build-wheel.yml
      - python-build-sdist.yml
      - python-deptry.yml
      - python-docformatter.yml
      - python-import-linter.yml
      - python-isort.yml
      - python-mypy.yml
      - python-pyroma.yml
      - python-pytest.yml
      - python-twine-upload.yml
      - python-vulture.yml
```

### All hooks

Add the following to your `.pre-commit-config.yaml` file to enable all targets:

```yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/python
    hooks:
      - id: python-autoflake
      - id: python-black
      - id: python-deptry
      - id: python-docformatter
      - id: python-import-linter
      - id: python-isort
      - id: python-pip-compile
      - id: python-pyroma
      - id: python-vulture
```

### Static analysis tools

The following linters are available for use:

```yaml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-autoflake.yml
      - python-black.yml
      - python-isort.yml
      - python-mypy.yml
      - python-pyroma.yml
      - python-vulture.yml
```

### Run unit tests

Run pytest unit tests:

```yaml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-pytest.yml
```

### Customize the behavior of `autoflake`

The following settings are the default options. Paste into your CI file to
customize:

```yaml
variables:
  PYTHON_AUTOFLAKE_OPTS: >-
    --remove-duplicate-keys
    --remove-unused-variables
    --expand-star-imports
    --remove-all-unused-imports
```

### Use `black` and `isort` together

The following minimal configuration is required in your `pyproject.toml` file:

```toml
[tool.isort]
profile = "black"
```

`known_first_party` is also recommended to use the linter without a complete
local environment:

```toml
[tool.isort]
known_first_party = ["python_pipeline"]
profile = "black"
```

### Build and publish

Build a package with setuptools and publish the package to a PyPI repository.

```yaml
include:
  - project: buildgarden/pipelines/python
    file:
      - python-build-sdist.yml
      - python-build-wheel.yml
      - python-twine-upload.yml
```

### Publish the package

By default, this pipeline publishes to GitLab's PyPI Package Registry.

The pipeline can be used to publish to PyPI by setting the following variables:

- `PYTHON_PYPI_UPLOAD_URL` = `https://upload.pypi.org/legacy/`
- `PYTHON_PYPI_USERNAME` = `__token__`
- `PYTHON_PYPI_PASSWORD` - Your API token

### Using Packages

#### Locally

Create the config directory:

```
mkdir -p ~/.config/pip
```

Create a `pip.conf` file:

```
$ cat ~/.config/pip/pip.conf
[global]
index-url = https://__token__:<read_api-token>gitlab.com/api/v4/groups/<group-id>/-/packages/pypi/simple
```

Install a package. GitLab will pull from [pypi.org](https://pypi.org) if the
package is not found.

```
$ pip install python-decouple
Looking in indexes: https://__token__:****@gitlab.com/api/v4/projects/32517826/packages/pypi/simple
Collecting python-decouple
  Downloading python_decouple-3.5-py3-none-any.whl (9.6 kB)
Installing collected packages: python-decouple
Successfully installed python-decouple-3.5
```

#### In CI

The most sensible way to use packages from the GitLab Package Registry is to
pull them from the root namespace registry. This ensures that any packages your
user may have access to are available for use.

The `CI_JOB_TOKEN` cannot be used to determine the group ID by name, so it must
be provided upfront. This is preferred to using a deploy token, so that the
permissions are specific to your user and not blanket.

Define the `PYTHON_PYPI_GITLAB_GROUP_ID` variable in your CI file, or ask a
group owner to add it to CI variables.

## Targets

| Name  | [GitLab include](https://docs.gitlab.com/ee/ci/yaml/includes.html) | [pre-commit hook](https://pre-commit.com/) | Description |
| ----- | ------- | ---- | ----------- |
| [helm-docs](#helm-docs) | ✓ | ✓ | Generate a chart README with [helm-docs](https://github.com/norwoodj/helm-docs). |
| [helm-kubeconform](#helm-kubeconform) | ✓ | ✓ | Validate chart manifests with [kubeconform](https://github.com/yannh/kubeconform). |
| [helm-lint](#helm-lint) | ✓ | ✓ | Check for common chart problems with [helm lint](https://helm.sh/docs/helm/helm_lint/). |
| [helm-package](#helm-package) | ✓ |  | Build a chart archive with [helm package](https://helm.sh/docs/helm/helm_package/). |
| [helm-push-chartmuseum](#helm-push-chartmuseum) | ✓ |  | Publish to ChartMuseum with [cm-push](https://github.com/chartmuseum/helm-push). |
| [helm-push-registry-artifacthub](#helm-push-registry-artifacthub) | ✓ |  | Publish Artifact Hub metadata to a container registry with [oras](https://github.com/oras-project/oras). |
| [helm-push-registry](#helm-push-registry) | ✓ |  | Publish a chart to a container registry with [helm push](https://helm.sh/docs/helm/helm_push/). |
| [helm-trivy](#helm-trivy) | ✓ | ✓ | Scan chart manifests for vulnerabilities with [trivy](https://github.com/aquasecurity/trivy). |

### `helm-docs`

Generate a chart README with [helm-docs](https://github.com/norwoodj/helm-docs).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-docs.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/helm
    rev: ""
    hooks:
      - id: helm-docs
```

### `helm-kubeconform`

Validate chart manifests with [kubeconform](https://github.com/yannh/kubeconform).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-kubeconform.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/helm
    rev: ""
    hooks:
      - id: helm-kubeconform
```

### `helm-lint`

Check for common chart problems with [helm lint](https://helm.sh/docs/helm/helm_lint/).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-lint.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/helm
    rev: ""
    hooks:
      - id: helm-lint
```

### `helm-package`

Build a chart archive with [helm package](https://helm.sh/docs/helm/helm_package/).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-package.yml
```

### `helm-push-chartmuseum`

Publish to ChartMuseum with [cm-push](https://github.com/chartmuseum/helm-push).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-push-chartmuseum.yml
```

### `helm-push-registry`

Publish a chart to a container registry with [helm push](https://helm.sh/docs/helm/helm_push/).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-push-registry.yml
```

### `helm-push-registry-artifacthub`

Publish Artifact Hub metadata to a container registry with [oras](https://github.com/oras-project/oras).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-push-registry-artifacthub.yml
```

### `helm-trivy`

Scan chart manifests for vulnerabilities with [trivy](https://github.com/aquasecurity/trivy).

As a GitLab include:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/helm
    file:
      - helm-trivy.yml
```

As a pre-commit hook:

```yaml
# .pre-commit-config.yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/helm
    rev: ""
    hooks:
      - id: helm-trivy
```

## Variables

### `HELM_APP_VERSION`

The chart `appVersion` to publish.

_Default:_ `$CI_COMMIT_TAG`

This pipeline overrides the `appVersion` field in the `Chart.yaml` file. This
ensures that the published chart version matches the tag it was released
from.

### `HELM_CHART_PATH`

The path of the chart to publish.

_Default:_ `$CI_PROJECT_DIR/chart`

### `HELM_CHART_VERSION`

The chart `version` to publish.

_Default:_ `$CI_COMMIT_TAG`

This pipeline overrides the `version` field in the `Chart.yaml` file. This
ensures that the published chart version matches the tag it was released
from.

### `HELM_KUBECONFORM_OPTS`

Options to pass directly to the `kubeconform` command.

_Default:_ `-strict -ignore-missing-schemas`

### `HELM_PACKAGE_DESTINATION`

Path of built Helm chart.

_Default:_ `$CI_PROJECT_DIR/dist/helm`

The path where the resulting Helm chart will be written. Useful if the
final location conflicts with another path in your project.

### `HELM_PACKAGE_OPTS`

Options to pass directly to the `helm package` command.

### `HELM_REGISTRY`

The full Docker registry image path to publish to.

_Default:_ `$CI_REGISTRY_IMAGE`

### `HELM_REGISTRY_HOST`

The Docker registry domain to authenticate with.

_Default:_ `$CI_REGISTRY`

### `HELM_REGISTRY_PASSWORD`

The password used to push to a Docker registry.

_Default:_ `$CI_REGISTRY_PASSWORD`

### `HELM_REGISTRY_USER`

The username used to push to a Docker registry.

_Default:_ `$CI_REGISTRY_USER`

### `HELM_REPOSITORY_PASSWORD`

The password used to push to a ChartMuseum repository.

_Default:_ `$CI_JOB_TOKEN`

### `HELM_REPOSITORY_URL`

The URL of the ChartMuseum repository to push to.

_Default:_ `$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/helm/production`

### `HELM_REPOSITORY_USERNAME`

The username used to push to a ChartMuseum repository.

_Default:_ `gitlab-ci-token`
